# 
from tools import *
import Vortex

# A simple controller that converts normalized inputs to gear ratio.
# A simple controller that off loads the driving shaft if its RPM goes under a critical value.
#
def on_simulation_start(extension):

    extension.iPump_Gain = create_input(extension, "Pump Gain", Vortex.Types.Type_VxReal, 0)
    extension.iMotor_1_Gain = create_input(extension, "Motor 1 Gain", Vortex.Types.Type_VxReal, 0)
    extension.iMotor_2_Gain = create_input(extension, "Motor 2 Gain", Vortex.Types.Type_VxReal, 0)
    extension.iShaftRPM = create_input( extension, "Engine RPM", Vortex.Types.Type_VxReal, 500)
    
    extension.oRatio_Pump = create_output(extension, "Ratio Pump", Vortex.Types.Type_VxReal, 0)
    extension.oRatio_1 = create_output(extension, "Ratio Motor 1", Vortex.Types.Type_VxReal, 0.5)
    extension.oRatio_2 = create_output(extension, "Ratio Motor 2", Vortex.Types.Type_VxReal, 0.5)

    extension.pMinShaftSpeed = create_parameter( extension, "Min Engine RPM", Vortex.Types.Type_VxReal, 500)
    
    extension.iPump_Gain.setDescription("Normalized input [0,1] to drive the pump")
    extension.iMotor_1_Gain.setDescription("Normalized input [-1,1] to drive the motor 1")
    extension.iMotor_2_Gain.setDescription("Normalized input [-1,1] to drive the motor 2")
    extension.iShaftRPM.setDescription( "RPM of the driving shaft" ) 
    extension.oRatio_Pump.setDescription("Output gear ratio applied to the pump")
    extension.oRatio_1.setDescription("Output gear ratio applied to motor 1")
    extension.oRatio_2.setDescription("Output gear ratio applied to motor 2")
    
    extension.pMinShaftSpeed.setDescription("Input shaft RPM where correction starts to be applied to reduce load on the driving shaft")
    
    # linear interpolation for inputs between -1 and 1 to outputs value between -1 and 1
    extension.input_vs_ratio = LinearInterpolation( [-1.0, 1.0],
                                               [-1.0, 1.0])
    
def pre_step(extension):

    # speed controller
    ratio = extension.input_vs_ratio(clamp(extension.iPump_Gain.value, 0.0, 1.0))
    
    # speed controller correction
    ratio_scale = 1.0
    ispeed = clamp( extension.iShaftRPM.value, 0.0, Vortex.VX_INFINITY)
    if extension.pMinShaftSpeed.value > 1.e-6 :
        ratio_scale = clamp( (ispeed/extension.pMinShaftSpeed.value) * (ispeed/extension.pMinShaftSpeed.value), 0.0, 1.0 )
    else:
        ratio_scale = 1.0
        
    extension.oRatio_Pump.value = ratio * ratio_scale
    
    # steering controller
    ratio = extension.input_vs_ratio(clamp(extension.iMotor_1_Gain.value, -1.0, 1.0))
    extension.oRatio_1.value = ratio
    ratio = extension.input_vs_ratio(clamp(extension.iMotor_2_Gain.value, -1.0, 1.0))
    extension.oRatio_2.value = ratio
