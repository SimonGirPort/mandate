# This script provides a simplified interface to define the locations of drivetrain components 
# and wheels of a vehicle. Drivetrain components are placed in a line starting from the 
# specified location. Wheels are positioned symmetrical to the vehicle centre based on the 
# specified dimensions. The script assumes the vehicle is x-forward

from tools import *
import math
import Vortex

def on_simulation_start(extension):

    create_input(extension, 'Enable', Vortex.Types.Type_Bool).setDescription("True to update positions")
    create_parameter(extension, 'Drivetrain Position', Vortex.Types.Type_VxVector3).setDescription("Position relative to the Chassis to place the first drivetrain component")
    create_parameter(extension, 'Drivetrain Spacing', Vortex.Types.Type_VxReal).setDescription("Each successive drivetrain component is placed this distance in front of the previous")
    create_parameter(extension, 'Wheel Position Front', Vortex.Types.Type_VxReal).setDescription("Position of front axle")
    create_parameter(extension, 'Wheel Position Rear', Vortex.Types.Type_VxReal).setDescription("Position of rear axle")
    create_parameter(extension, 'Wheel Spacing', Vortex.Types.Type_VxReal).setDescription("Distance between left and right wheels")
    create_parameter(extension, 'Wheel Height', Vortex.Types.Type_VxReal).setDescription("Vertical position of wheels")

    create_output(extension, 'Engine Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Clutch Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Transmission Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Differential Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Wheel FL Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Wheel FR Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Wheel RL Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Wheel RR Transform', Vortex.Types.Type_VxMatrix44)

def paused_update(extension):
    config_update(extension)

def config_update(extension):
    # Skip update if disabled
    if not extension.inputs.Enable.value:
        return

    pos = extension.parameters.Drivetrain_Position.value
    extension.outputs.Engine_Transform.value = Vortex.createTranslation(pos)
    pos.x += extension.parameters.Drivetrain_Spacing.value
    extension.outputs.Clutch_Transform.value = Vortex.createTranslation(pos)
    pos.x += extension.parameters.Drivetrain_Spacing.value
    extension.outputs.Transmission_Transform.value = Vortex.createTranslation(pos)
    pos.x += extension.parameters.Drivetrain_Spacing.value
    extension.outputs.Differential_Transform.value = Vortex.createTranslation(pos)

    pos = Vortex.VxVector3(extension.parameters.Wheel_Position_Front.value, extension.parameters.Wheel_Spacing.value/2, extension.parameters.Wheel_Height.value)
    extension.outputs.Wheel_FL_Transform.value = Vortex.createTranslation(pos)
    pos.y = -pos.y
    extension.outputs.Wheel_FR_Transform.value = Vortex.createTranslation(pos)
    pos.x = extension.parameters.Wheel_Position_Rear.value
    extension.outputs.Wheel_RR_Transform.value = Vortex.createTranslation(pos)
    pos.y = -pos.y
    extension.outputs.Wheel_RL_Transform.value = Vortex.createTranslation(pos)
