# This script uses a set of control point inputs and calculates part and 
# constraint positions accordingly

from tools import *
import math
import Vortex

def on_simulation_start(extension):

    create_input(extension, 'Enable', Vortex.Types.Type_Bool).setDescription("True to update calculations")

    create_parameter(extension, 'LCA Rear Position', Vortex.Types.Type_VxVector3).setDescription("Position of lower control arm rear bushing")
    create_parameter(extension, 'LCA Front Position', Vortex.Types.Type_VxVector3).setDescription("Position of lower control arm front bushing")
    create_parameter(extension, 'LCA Outer Position', Vortex.Types.Type_VxVector3).setDescription("Position of lower control arm outer bushing")
    create_parameter(extension, 'UCA Rear Position', Vortex.Types.Type_VxVector3).setDescription("Position of upper control arm rear bushing")
    create_parameter(extension, 'UCA Front Position', Vortex.Types.Type_VxVector3).setDescription("Position of upper control arm front bushing")
    create_parameter(extension, 'UCA Outer Position', Vortex.Types.Type_VxVector3).setDescription("Position of upper control arm outer bushing")
    create_parameter(extension, 'Strut Upper Position', Vortex.Types.Type_VxVector3).setDescription("Position of strut upper bushing")
    create_parameter(extension, 'Strut Lower Position', Vortex.Types.Type_VxVector3).setDescription("Position of strut lower bushing")
    create_parameter(extension, 'Reverse Y', Vortex.Types.Type_Bool).setDescription("Set true to mirror the suspension to the opposite side of the vehicle by reversing all the Y dimenions")

    # Part transforms
    create_output(extension, 'LCA Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'UCA Transform', Vortex.Types.Type_VxMatrix44)
    create_output(extension, 'Knuckle Transform', Vortex.Types.Type_VxMatrix44)

    # Constraint attachments
    create_output(extension, 'LCA Front Offset', Vortex.Types.Type_VxVector3)
    create_output(extension, 'LCA Primary', Vortex.Types.Type_VxVector3)
    create_output(extension, 'LCA Secondary', Vortex.Types.Type_VxVector3)
    create_output(extension, 'UCA Front Offset', Vortex.Types.Type_VxVector3)
    create_output(extension, 'Knuckle Upper Offset', Vortex.Types.Type_VxVector3)
    create_output(extension, 'UCA Primary', Vortex.Types.Type_VxVector3)
    create_output(extension, 'UCA Secondary', Vortex.Types.Type_VxVector3)
    create_output(extension, 'Strut Upper Position', Vortex.Types.Type_VxVector3)
    create_output(extension, 'Strut Lower Position', Vortex.Types.Type_VxVector3)

    # Starting info
    create_output(extension, 'Strut Initial Length', Vortex.Types.Type_VxReal)

def paused_update(extension):
    config_update(extension)

def config_update(extension):
    # Skip update if disabled
    if not extension.inputs.Enable.value:
        return

    # Positions of points
    lcar_pos = extension.parameters.LCA_Rear_Position.value
    lcaf_pos = extension.parameters.LCA_Front_Position.value
    lcao_pos = extension.parameters.LCA_Outer_Position.value
    ucar_pos = extension.parameters.UCA_Rear_Position.value
    ucaf_pos = extension.parameters.UCA_Front_Position.value
    ucao_pos = extension.parameters.UCA_Outer_Position.value
    su_pos = extension.parameters.Strut_Upper_Position.value
    sl_pos = extension.parameters.Strut_Lower_Position.value

    # Reverse - negate y value to shift from left to right
    if extension.parameters.Reverse_Y.value:
        lcar_pos.y = -lcar_pos.y
        lcaf_pos.y = -lcaf_pos.y
        lcao_pos.y = -lcao_pos.y
        ucar_pos.y = -ucar_pos.y
        ucaf_pos.y = -ucaf_pos.y
        ucao_pos.y = -ucao_pos.y
        su_pos.y = -su_pos.y
        sl_pos.y = -sl_pos.y

    # Calculate constraint directions
    # Hinge oriented from rear to front
    lca_prim = lcaf_pos - lcar_pos
    lca_prim.normalize()
    
    uca_prim = ucaf_pos - ucar_pos
    uca_prim.normalize()
    # Secondary direction points to outer point, orthoganal to primary direction
    lca_sec = lcao_pos - lcar_pos
    lca_sec.orthogonalize(lca_prim)
    lca_sec.normalize()
    
    uca_sec = ucao_pos - ucar_pos
    uca_sec.orthogonalize(uca_prim)
    uca_sec.normalize()


    # Part transforms
    extension.outputs.LCA_Transform.value = Vortex.createTranslation(lcar_pos)
    extension.outputs.UCA_Transform.value = Vortex.createTranslation(ucar_pos)
    extension.outputs.Knuckle_Transform.value = Vortex.createTranslation(lcao_pos)

    # Constraint attachments
    extension.outputs.LCA_Front_Offset.value = lcaf_pos - lcar_pos # Front position is offset from part origin, which is rear position
    extension.outputs.LCA_Primary.value = lca_prim
    extension.outputs.LCA_Secondary.value = lca_sec

    extension.outputs.UCA_Front_Offset.value = ucaf_pos - ucar_pos # Front position is offset from part origin, which is rear position
    extension.outputs.Knuckle_Upper_Offset.value = ucao_pos - lcao_pos # Upper knuckle position is offset from part origin, which is buttom knuckle position
    extension.outputs.UCA_Primary.value = uca_prim
    extension.outputs.UCA_Secondary.value = uca_sec

    extension.outputs.Strut_Upper_Position.value = su_pos - lcar_pos
    extension.outputs.Strut_Lower_Position.value = sl_pos - lcar_pos

    extension.outputs.Strut_Initial_Length.value = (su_pos - sl_pos).norm()