# This script sets the mass and (spherical) moment of inertia of the specified part

from tools import *
import Vortex

def on_simulation_start(extension):
    create_input(extension, "Mass", Vortex.Types.Type_VxReal)
    create_input(extension, "Moment of Inertia", Vortex.Types.Type_VxReal)

    create_parameter(extension, "Part", Vortex.Types.Type_Part)

    extension.inputs.Mass.setDescription("Mass of output shaft in kg")
    extension.inputs.Moment_of_Inertia.setDescription("Moment of inertia of output shaft, assumed spherical in kg.m^2")
    extension.parameters.Part.setDescription("Part to apply mass and moment of inertia")

def pre_step(extension):
    set_mass(extension)

def paused_update(extension):
    set_mass(extension)

def set_mass(extension):
    properties = extension.parameters.Part.value.parameterMassPropertiesContainer
    properties.mass.value = max(extension.inputs.Mass.value, 0.000001)

    MOI = max(extension.inputs.Moment_of_Inertia.value, 0.000001)
    properties.inertiaTensor.value = Vortex.Matrix44(MOI, 0, 0, 0, 
                                                     0, MOI, 0, 0, 
                                                     0, 0, MOI, 0, 
                                                     0, 0, 0, 0)