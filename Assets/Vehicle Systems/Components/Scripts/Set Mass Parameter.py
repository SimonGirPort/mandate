# This script sets the mass and (spherical) moment of inertia of the specified part

from tools import *
import Vortex

def on_simulation_start(extension):
    create_parameter(extension, "Mass", Vortex.Types.Type_VxReal)
    create_parameter(extension, "Moment of Inertia", Vortex.Types.Type_VxReal)
    create_parameter(extension, "Part", Vortex.Types.Type_Part)

    extension.parameters.Mass.setDescription("Mass of output shaft in kg")
    extension.parameters.Moment_of_Inertia.setDescription("Moment of inertia of output shaft, assumed spherical in kg.m^2")
    extension.parameters.Part.setDescription("Part to apply mass and moment of inertia")

def paused_update(extension):
    set_mass(extension)

def set_mass(extension):
    properties = extension.parameters.Part.value.parameterMassPropertiesContainer
    properties.mass.value = max(extension.parameters.Mass.value, 0.000001)

    MOI = max(extension.parameters.Moment_of_Inertia.value, 0.000001)
    properties.inertiaTensor.value = Vortex.Matrix44(MOI, 0, 0, 0, 
                                                      0, MOI, 0, 0, 
                                                      0, 0, MOI, 0, 
                                                      0, 0, 0, 0)